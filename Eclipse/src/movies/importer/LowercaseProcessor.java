// David Tran 1938381

package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	public LowercaseProcessor(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	
	
	public ArrayList<String> process(ArrayList<String> arrayListInput){
		
		ArrayList<String> asLower = new ArrayList<String>();
		
		for (int count = 0; count < arrayListInput.size(); count++) {
			String copyString = arrayListInput.get(count);
			asLower.add(copyString.toLowerCase());
		}
		
		return asLower;
	}
	
}