// David Tran 1938381

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{

	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> arrayListInput){
		ArrayList<String> noDups = new ArrayList<String>();
		
		for (int count = 0; count < arrayListInput.size(); count++) {
			boolean noDupAnswer = true;
			for (int secondCount = count + 1; secondCount < arrayListInput.size(); secondCount++) {
				if(arrayListInput.get(count).equals(arrayListInput.get(secondCount))) {
					noDupAnswer = false;
				}
			}
			if(noDupAnswer) {
				noDups.add(arrayListInput.get(count));
			}
		}
		return noDups;
	}

}