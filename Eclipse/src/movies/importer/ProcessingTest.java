// David Tran 1938381

package movies.importer;

import java.io.*;

public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		
		String sourceOne = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Assignments\\Java\\Assignment_5\\Source";
		String destinationOne = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Assignments\\Java\\Assignment_5\\Destination";
		
		LowercaseProcessor newLowercaseProcessor = new LowercaseProcessor(sourceOne, destinationOne);
		newLowercaseProcessor.execute();
		
		
		
		String sourceTwo = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Assignments\\Java\\Assignment_5\\Destination";
		String destinationTwo = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Assignments\\Java\\Assignment_5\\Final_Destination";
		
		RemoveDuplicates newRemoveDuplicatesProcessor = new RemoveDuplicates(sourceTwo, destinationTwo);
		newRemoveDuplicatesProcessor.execute();
		
	}

}